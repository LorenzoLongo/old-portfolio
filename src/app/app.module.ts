import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MenuComponent } from './core/menu/menu.component';
import {RouterModule} from '@angular/router';
import {ContactComponent} from './contact/contact.component';
import { PersonallyComponent } from './personally/personally.component';
import { FooterComponent } from './core/footer/footer.component';
import { SkillsComponent } from './skills/skills.component';
import { PortfolioComponent } from './portfolio/portfolio.component';


const appRoutes = [
  { path: '', component: MenuComponent },
  { path: 'about-me', component: PersonallyComponent },
  { path: 'skills', component: SkillsComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ContactComponent,
    PersonallyComponent,
    FooterComponent,
    SkillsComponent,
    PortfolioComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
